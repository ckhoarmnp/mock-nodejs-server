const admin = require('firebase-admin')
const { google } = require('googleapis')
const axios = require('axios')

const MESSAGING_SCOPE = 'https://www.googleapis.com/auth/firebase.messaging'
const SCOPES = [MESSAGING_SCOPE]

const serviceAccount = require('./testfirebase-d7515-firebase-adminsdk-k3tqm-019f3a3712.json')
const databaseURL = 'https://testfirebase-d7515.firebaseio.com'
const URL =
  'https://fcm.googleapis.com/v1/projects/testfirebase-d7515/messages:send'
const deviceToken =
  'cXmowfIDoJM:APA91bHFwakUzKGbJsurzMWh4AX2MZOnVD6EZSNtPShZRuxay9oRqhEWc-R_1vi7Bavonh_OKj3_k2M_-yh6x9B_8G6WL6k1lkj8f8wzMit2XOVsg2Jg1zPgu5ZyVpN_DDOcHZr4tIZr'

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: databaseURL
})

function getAccessToken() {
  return new Promise(function(resolve, reject) {
    var key = serviceAccount
    var jwtClient = new google.auth.JWT(
      key.client_email,
      null,
      key.private_key,
      SCOPES,
      null
    )
    jwtClient.authorize(function(err, tokens) {
      if (err) {
        reject(err)
        return
      }
      resolve(tokens.access_token)
    })
  })
}

async function init() {
  const body = {
    message: {
      data: { key: 'value' },
      notification: {
        title: 'Notification title',
        body: 'Notification body'
      },
      webpush: {
        headers: {
          Urgency: 'high'
        },
        notification: {
          requireInteraction: 'true'
        }
      },
      token: deviceToken
    }
  }

  try {
    const accessToken = await getAccessToken()
    console.log('accessToken: ', accessToken)
    const { data } = await axios.post(URL, JSON.stringify(body), {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${accessToken}`
      }
    })
    console.log('name: ', data.name)
  } catch (err) {
    console.log('err: ', err.message)
  }
}

init()